package com.portoseg.invoice.controller;

import javax.validation.constraints.NotBlank;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.portoseg.invoice.request.CustomerRequest;
import com.portoseg.invoice.response.InvoiceIdResponse;
import com.portoseg.invoice.service.InvoiceService;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/")
@RequiredArgsConstructor
public class InvoiceController {

	private final InvoiceService invoiceService;
	
	@PatchMapping("customers/{customerId}")
	public ResponseEntity<Void> updateInvoiceDeliveryFromCustomer(
			@RequestBody @NotBlank CustomerRequest invoiceRequest,
			@PathVariable(name = "customerId") String customerId) {
		invoiceService.updateInvoiceFromCustomer(invoiceRequest, customerId);
		return ResponseEntity.accepted().build();
	}
	
	@GetMapping("invoices/{invoiceId}")
	public ResponseEntity<InvoiceIdResponse> getInvoiceById (
			@PathVariable(name = "invoiceId") String invoiceId){
		return ResponseEntity.ok(invoiceService.getInvoiceById(invoiceId));
	}
}
