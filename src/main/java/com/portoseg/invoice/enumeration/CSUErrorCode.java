package com.portoseg.invoice.enumeration;

import java.util.stream.Stream;

import com.portoseg.invoice.config.MessageConfig;
import com.portoseg.invoice.exception.http.CSUUnavailableException;
import com.portoseg.invoice.exception.http.InvalidAccountException;
import com.portoseg.invoice.response.ErrorResponse;

public enum CSUErrorCode {

	INVALID_OR_NOT_REGISTERED_ACCOUNT("003-000", new InvalidAccountException());
	
	private String code;
	private RuntimeException exception;

	private CSUErrorCode(String code, RuntimeException exception) {
		this.code = code;
		this.exception = exception;
	}
	
	public String getCode() {
		return code;
	}
	
	public RuntimeException getException() {
		return exception;
	}
	
	public static RuntimeException getExceptionFromCode(String code, String message) {
		return Stream.of(values()).filter(error -> error.getCode().equals(code))
				.map(CSUErrorCode::getException)
				.findFirst()
				.orElseGet(() -> new CSUUnavailableException(ErrorResponse.builder()
												.code("422.000")
												.message((new MessageConfig()).getMessage("422.000"))
												.detail(message)
												.build()));
	}
}
