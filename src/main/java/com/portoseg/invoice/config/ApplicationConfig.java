package com.portoseg.invoice.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import lombok.Data;

@Data
@Configuration
public class ApplicationConfig {
	@Value("${manager.token.endpoint}")
	private String tokenEndpoint;

	@Value("${csu.token.endpoint}")
	private String csuTokenEndpoint;
	
	@Value("${manager.customers.patch.customer}")
	private String patchInvoiceEndpoint;

	@Value("${manager.invoices.get.invoice-id}")
	private String getInvoiceByIdEndpoint;
	
	@Value("${manager.client-id}")
	private String clientId;

	@Value("${manager.client-secret}")
	private String clientSecret;
	
	@Value("${spring.redis.cache.timeout}")
	private Integer redisCacheTimeout;
	
	@Value("${spring.redis.key-prefix.card}")
 	private String redisCardKeyPrefix;
}
