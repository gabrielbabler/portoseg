package com.portoseg.invoice.config;

import org.springframework.context.annotation.Configuration;

import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.RequiredArgsConstructor;

@Configuration
@RequiredArgsConstructor
public class JsonConfig {

	private final ObjectMapper mapper;

	public <T> T stringJsonToObject(String json, Class<T> clazz) {
		try {
			return mapper.readValue(json, clazz);
		} catch (Exception e) {
			throw new IllegalStateException();
		}
	}

	public String objectToStringJson(Object object) {
		try {
			return mapper.writeValueAsString(object);
		} catch (Exception e) {
			throw new IllegalStateException();
		}
	}
}
