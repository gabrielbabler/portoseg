package com.portoseg.invoice.exception.http;

import org.springframework.http.HttpStatus;

import com.portoseg.invoice.response.ErrorResponse;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class UnprocessableEntityException extends RestException {
	
	private static final long serialVersionUID = 3980240545669347957L;
	
	private ErrorResponse errorResponse;
	
	public UnprocessableEntityException(ErrorResponse errorResponse) {
		this.errorResponse = errorResponse;
	}
	
	@Override
	public HttpStatus getStatus() {
		return HttpStatus.UNPROCESSABLE_ENTITY;
	}

	@Override
	public Object getBody() {
		return errorResponse;
	}
}
