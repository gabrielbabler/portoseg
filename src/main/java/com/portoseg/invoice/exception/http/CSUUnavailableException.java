package com.portoseg.invoice.exception.http;

import com.portoseg.invoice.response.ErrorResponse;

public class CSUUnavailableException extends UnprocessableEntityException {
	private static final long serialVersionUID = -7966799626352135416L;
	
	public CSUUnavailableException(ErrorResponse errorResponse) {
		super(errorResponse);
	}
}
