package com.portoseg.invoice.exception.http;

import org.springframework.http.HttpStatus;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class InternalServerErrorException extends RestException {

	private static final long serialVersionUID = 7491514333398467945L;
	
	@Override
	public HttpStatus getStatus() {
		return HttpStatus.INTERNAL_SERVER_ERROR;
	}

	public InternalServerErrorException(String message) {
		super(message);
	}
}
