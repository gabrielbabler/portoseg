package com.portoseg.invoice.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.portoseg.invoice.domain.InvoiceDomain;

@Repository
public interface InvoiceRepository extends CrudRepository<InvoiceDomain, String>{
	
	public static List<InvoiceDomain> getInvoicesByFilter() {
		return null;
	}
}
