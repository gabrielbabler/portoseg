package com.portoseg.invoice.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.portoseg.invoice.domain.InvoiceDomain;
import com.portoseg.invoice.exception.http.NotFoundException;
import com.portoseg.invoice.repository.InvoiceRepository;
import com.portoseg.invoice.request.CustomerRequest;
import com.portoseg.invoice.response.InvoiceIdResponse;
import com.portoseg.invoice.response.InvoiceResponse;
import com.portoseg.invoice.service.client.CSUClient;
import com.portoseg.invoice.service.client.RedisClient;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class InvoiceService {
	
	private final CSUClient csuClient;
	private final RedisClient redisClient;
	private final InvoiceRepository invoiceRepository;
	
	StringBuilder logs = new StringBuilder();
	
	public void updateInvoiceFromCustomer(CustomerRequest invoiceRequest, String customerId) {
		csuClient.updateInvoiceDeliveryOfCustomer(invoiceRequest, customerId, logs);
		System.out.println(logs);
	}
	
	public InvoiceIdResponse getInvoiceById(String invoiceId) {
		return redisClient.get(invoiceId, InvoiceIdResponse.class).orElseThrow(NotFoundException::new);
	}

	public List<InvoiceResponse> getInvoices() {
		Iterable<InvoiceDomain> invoices = invoiceRepository.findAll();
		
		return null;
	}
}