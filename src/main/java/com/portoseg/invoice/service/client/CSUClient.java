package com.portoseg.invoice.service.client;

import java.time.LocalDateTime;
import java.util.Base64;

import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;

import com.portoseg.invoice.config.ApplicationConfig;
import com.portoseg.invoice.enumeration.CSUErrorCode;
import com.portoseg.invoice.request.CustomerRequest;
import com.portoseg.invoice.response.CSUTokenResponse;
import com.portoseg.invoice.response.TokenResponse;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class CSUClient {

	private final ApplicationConfig applicationConfig;
	private final RestTemplateBuilder rest;
	private static final TokenResponse TOKEN = TokenResponse.builder().creationDate(LocalDateTime.now()).expiresIn(0)
			.build();

	public void updateInvoiceDeliveryOfCustomer(CustomerRequest invoiceRequest, String customerId, StringBuilder logs) {
		logs.append("Updating invoice delivery");
		
		HttpHeaders header = new HttpHeaders();
		header.add("access_token", getAccessToken(logs));
		header.add("client_id", applicationConfig.getClientId());
		header.add("Content-Type", "application/json");
		
		HttpEntity<?> entity = new HttpEntity<>(
				"{" 
				+ "\"NumeroConta\":\""+ customerId + "\","
				+ "\"TipoEnvio\":\""+ invoiceRequest.getDeliveryType() +
				"\"}",
				header);
		try {
			ResponseEntity<Void> exchange = rest.build().exchange(applicationConfig.getPatchInvoiceEndpoint(), HttpMethod.PUT, entity,
					Void.class);
			
			logs.append("código retorno csu(put) - " +exchange.getStatusCode());
			
		} catch (HttpStatusCodeException e) {
			int codeIndex = StringUtils.indexOf(e.getResponseBodyAsString(), "COD: ") + 5;
			String errorCode = StringUtils.substring(e.getResponseBodyAsString(), codeIndex, codeIndex + 7);
			throw CSUErrorCode.getExceptionFromCode(errorCode, e.getResponseBodyAsString());
		}
	}

	private void generateToken(StringBuilder logs) {
		logs.append("Generating token");
		
		HttpHeaders header = new HttpHeaders();
		header.add("Authorization", "Basic " + Base64.getEncoder().encodeToString(
				(applicationConfig.getClientId() + ":" + applicationConfig.getClientSecret()).getBytes()));
		header.add("Content-Type", "application/json");
		
		HttpEntity<?> entity = new HttpEntity<>("{\"grant_type\":\"client_credentials\"}",header);
		
		ResponseEntity<CSUTokenResponse> token = rest.build().postForEntity(applicationConfig.getCsuTokenEndpoint(),
				entity, CSUTokenResponse.class);

		TOKEN.setExpiresIn(token.getBody().getExpiresIn());
		TOKEN.setAccessToken(token.getBody().getAccessToken());
		TOKEN.setCreationDate(LocalDateTime.now());
		
		System.out.println("Logs: " + logs);
	}

	private String getAccessToken(StringBuilder logs) {
		if (isTokenInvalid()) {
			logs.append("Token invalid, generating new token");
			generateToken(logs);
		}
		return TOKEN.getAccessToken();
	}

	private boolean isTokenInvalid() {
		LocalDateTime expirationDate = TOKEN.getCreationDate().plusSeconds(TOKEN.getExpiresIn());
		return expirationDate.isBefore(LocalDateTime.now());
	}
}
