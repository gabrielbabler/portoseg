package com.portoseg.invoice.service.client;

import java.util.Optional;
import java.util.concurrent.TimeUnit;

import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import com.portoseg.invoice.config.ApplicationConfig;
import com.portoseg.invoice.config.JsonConfig;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class RedisClient {

	private final StringRedisTemplate redis;

	private final ApplicationConfig applicationConfig;

	private final JsonConfig jsonConfig;

	public void set(String key, Object value) {
		redis.opsForValue().set(key, jsonConfig.objectToStringJson(value), applicationConfig.getRedisCacheTimeout(),
				TimeUnit.HOURS);
	}

	public <T> Optional<T> get(String key, Class<T> clazz) {
		return Optional.ofNullable(redis.opsForValue().get(applicationConfig.getRedisCardKeyPrefix() + key))
				.map(value -> jsonConfig.stringJsonToObject(value, clazz));
	}
}
