package com.portoseg.invoice.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;

import com.portoseg.invoice.response.DeliveryInfoResponse;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@RedisHash("invoice")
public class InvoiceDomain {
	
		@Id
		private String id;
		private String cutDate;
		private String dueDate;
		private Integer value;
		private String logo;
		private DeliveryInfoResponse deliveryInfo;
		private String invoiceType;
		private String limit;
		private String offset;
		private String expand;

}
