package com.portoseg.invoice.response;

import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TokenResponse {
	private String accessToken;
	private String tokenType;
	private Integer expiresIn;
	private LocalDateTime creationDate;
}
