package com.portoseg.invoice.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class InvoiceIdResponse {

	@JsonProperty("typeable_line")
	private String typeableLine;
	
	@JsonProperty("effective_statement_date")
	private String effectiveStatementDate;
	
	@JsonProperty("closing_invoice_date")
	private String closingInvoiceDate;
	
	@JsonProperty("due_date")
	private String dueDate;
	
	@JsonProperty("previous_balance")
	private String previousBalance;
	
	@JsonProperty("credit_payments")
	private String creditPayments;
	
	@JsonProperty("purchase_expenses")
	private String purchaseExpenses;
	
	@JsonProperty("invoice_charges")
	private String invoiceCharges;
	
	@JsonProperty("fine_delay")
	private String fineDelay;
	
	@JsonProperty("total_amount")
	private String totalAmount;
	
	@JsonProperty("minimum_payment")
	private String minimumPayment;
	
	@JsonProperty("minimum_safe_payment")
	private String minimumSafePayment;
	
	@JsonProperty("transactions")
	private List<TransactionResponse> transactions;
}
