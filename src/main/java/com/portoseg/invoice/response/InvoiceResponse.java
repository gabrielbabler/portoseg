package com.portoseg.invoice.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class InvoiceResponse {

	@JsonProperty("id")
	private String id;
	
	@JsonProperty("cut_date")
	private String cutDate;
	
	@JsonProperty("due_date")
	private String dueDate;
	
	@JsonProperty("value")
	private Integer value;
	
	@JsonProperty("logo")
	private String logo;
	
	@JsonProperty("delivery_info")
	private DeliveryInfoResponse deliveryInfo;
	
	@JsonProperty("invoiceType")
	private String invoiceType;
}
