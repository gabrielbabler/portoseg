package com.portoseg.invoice.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TransactionResponse {

	@JsonProperty("date")
	private String date;
	
	@JsonProperty("launch_date")
	private String launchDate;
	
	@JsonProperty("description")
	private String description;
	
	@JsonProperty("real_value")
	private String realValue;
	
	@JsonProperty("installment_value_interest")
	private String installmentValueInterest;
	
	@JsonProperty("dollar_value")
	private String dollarValue;
	
	@JsonProperty("dollar_quote")
	private String dollarQuote;
	
	@JsonProperty("launch_type")
	private String launchType;
	
	@JsonProperty("authorization_code")
	private String authorizationCode;
	
	@JsonProperty("print_flag")
	private String printFlag;
	
	@JsonProperty("mcc_number")
	private String mccNumber;
	
	@JsonProperty("mcc_group")
	private String mccGroup;
	
	@JsonProperty("installment")
	private String installment;
	
	@JsonProperty("code")
	private String code;
	
	@JsonProperty("masked_card")
	private String maskedCard;
	
	@JsonProperty("origin_currency_purchase_amount")
	private String originCurrencyPurchaseAmount;
}
